FactoryBot.define do
    factory :activity do
        association :user, factory: :user
        activity_type {:cycling}
    end
end
