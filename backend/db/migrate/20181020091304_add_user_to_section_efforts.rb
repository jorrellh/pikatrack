class AddUserToSectionEfforts < ActiveRecord::Migration[5.2]
  def up
    add_reference :section_efforts, :user, index: true
    SectionEffort.all.each{|se| se.update(user_id: se.activity.user.id)}
    change_column_null :section_efforts, :user_id, false
  end

  def down
    remove_column :section_efforts, :user_id
  end
end
